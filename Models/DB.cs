using Microsoft.EntityFrameworkCore;
namespace TodoApi.Models
{
   public class DB:DbContext
   {
       public DbSet<TodoItem> todoes {set;get;}
       protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)

        {
            optionsBuilder.UseSqlite("Filename=./DB.db");
        }
    }
}