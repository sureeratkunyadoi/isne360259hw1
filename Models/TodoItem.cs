using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
namespace TodoApi.Models
{
    public class TodoItem
    {
        [KeyAttribute]
        public string Key { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }
    }
}